package com.sygic.nightmodeenabler

import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val currentStatus = Settings.Secure.getInt(contentResolver, "ui_night_mode", -1)
        result.text = "Status: $currentStatus"

        when (currentStatus) {
            0 -> night_auto.isChecked = true
            1 -> night_off.isChecked = true
            2 -> night_on.isChecked = true
        }
    }

    fun onRadioButtonClicked(view: View) {
        try {
            when (view.id) {
                R.id.night_auto -> Settings.Secure.putInt(contentResolver, "ui_night_mode", 0)
                R.id.night_off -> Settings.Secure.putInt(contentResolver, "ui_night_mode", 1)
                R.id.night_on -> Settings.Secure.putInt(contentResolver, "ui_night_mode", 2)
            }

            result.text = "Status: ${Settings.Secure.getInt(contentResolver, "ui_night_mode", -1)}"

        } catch (e: SecurityException) {
            Log.w("NightModeEnabler", "No access to secure settings obtained!")
            result.text = "Error: No access to secure settings obtained!\nStatus unchanged: ${Settings.Secure.getInt(contentResolver, "ui_night_mode", -1)}"
        }
    }
}
